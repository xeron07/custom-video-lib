
dependency=()=>{

    let script=document.createElement("script");
    script.src="dependency/strophe.min.js";
    document.head.appendChild(script); 

    script=document.createElement("script");
    script.src="dependency/strophe.disco.min.js";
    document.head.appendChild(script);

    script=document.createElement("script");
    script.src="dependency/lib-jitsi-meet.min.js";
    document.head.appendChild(script);

    script=document.createElement("script");
    script.src="https://cdn.jsdelivr.net/npm/sweetalert2@9";
    document.head.appendChild(script);
}

dependency();

class Conference{
    constructor(options){
        //// ** initial variables ** ////
        this.hostObj={
            hosts:{
                domain:"192.168.0.150",
                muc:"conference.192.168.0.150",
                anonymousdomain:"192.168.0.150"
               },
               bosh:"https://192.168.0.150/http-bind"
        };
        this.defaultObj={
            "server":this.hostObj,
            "width":"100%",
            "height":"100%",
            "node":null,
            "roomName":"abc"
        }
        this.config={...this.defaultObj, ...options};
        this.connection=null;
        this.remoteTracks=[];
        this.room=null;
        this.isJoined=false;
        this.members=[];
        this.roomName="abc";
        this.localTracks=[];
        this.initOptions=null;
        this.role=null;
        
    }
   

   //// ** get set functions  ** ////
   
   // ** method to get localTracks **//
   getLocaltracks=()=>{
       return this.localTracks;
   }

   // method for returning connection object
   getConnection=()=>{
       return this.connection;
   }

   //method to return room object
   getRoom=()=>{
       return this.room;
   }

   //Participants array data
   getMembers=()=>{
       return this.members;
   }

   //getting configuration

   //Setting conference room name
   setRoomName=(name)=>{
       this.roomName=name;
   }

   //configuration setting
   setConfiguration=(conf)=>{
       this.option=conf;
   }

   //user role setting
   setRole=(value)=>{
       this.role=value;
   }

   
    //// ** jitsi initialize option configuration ** ////
  browserOptions=()=>{
    this.initOptions={
         disableAudioLevels: false,
    
        // The ID of the jidesha extension for Chrome.
        desktopSharingChromeExtId: 'mbocklcggfhnbahlnepmldehdhpjfcjp',
    
        // Whether desktop sharing should be disabled on Chrome.
        desktopSharingChromeDisabled: false,
    
        // The media sources to use when using screen sharing with the Chrome
        // extension.
        desktopSharingChromeSources: [ 'screen', 'window' ],
    
        // Required version of Chrome extension
        desktopSharingChromeMinExtVersion: '0.1',
    
        // Whether desktop sharing should be disabled on Firefox.
        desktopSharingFirefoxDisabled: true
    };

    return this.initOptions;

   }

  //// ** conference configuration  ** ////
  // we didn't add any parameter for the time being //
  
   conferenceOption=()=>{   
    this.confOption={
       openBridgeChannel:true
     };
     return this.confOption;
   }


//// ** event functions ** ////
onJoinSuccess=()=>{
   if(this.createRoom()){
       this.conferenceEvents();
       this.joinRoom();
   }
}


// ** local track integration ** //
onLocalTracks=(tracks)=>{
    this.localTracks=tracks;
    let str='<div class="video-box" style=" position:relative;border-radius:5px;top:5%;left:95%;padding:20px;width:15%;"><video id="localVideo" autoplay="1">  </video><audio id="localAudio"></audio></div>';
    document.getElementById("black-box").innerHTML=str;
    for(let i=0;i<this.localTracks.length;i++){
        if(this.localTracks[i].getType()==="video"){
            this.localTracks[i].attach($("#localVideo")[0]);
        }else{
            this.localTracks[i].attach($("#localAudio")[0]);
        }
    }
}


//// ** local track handler ** ////
//** @param is a JitsiTrack **//
localTrack=(track)=>{
this.localTracks=track;
}

////** remote track handler **////
//** @param is a JitsiTrack object **//
remoteTrack=(track)=>{
if(track.isLocal()){
    return;
}

const participant=track.getParticipantId();

if(!this.remoteTracks[participant]){
    this.remoteTracks[participant]=[];
}

const idx=this.remoteTracks[participant].push(track)

let user={
    id:participant,
    vid:participant+idx,
    track:track,
    isKicked:false
};
console.clear();
console.log(track);
Swal.fire("track"+track);

this.members.push(user);

if(this.role==="teacher"){
    this.addStudents();
}else{
    this.addTeacher(2);
}

//return function will call here
}

//// ** conference events ** ////
conferenceEvents=()=>{
   this.room.on(JitsiMeetJS.events.conference.TRACK_ADDED,this.remoteTrack);
   this.room.on(JitsiMeetJS.events.conference.TRACK_REMOVED,()=>{
    Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"track has removed"});
   });
   this.room.on(JitsiMeetJS.events.conference.USER_JOINED,()=>{
    Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"a new user joined "});
   });
   this.room.on(JitsiMeetJS.events.conference.USER_LEFT,(a)=>{
       Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"uleft"+a});
   });
   this.room.on(JitsiMeetJS.events.conference.CONFERENCE_JOINED,()=>{
       Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"succes in join"})
   });
   this.room.on(JitsiMeetJS.events.conference.CONFERENCE_LEFT,()=>{
       Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"you have left the conference"});
   });
   this.room.on(JitsiMeetJS.events.conference.CONFERENCE_FAILED,(f)=>{
       console.error(f);
       Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"you can't join the conference"});
   });
   this.room.on(JitsiMeetJS.events.conference.KICKED,()=>{
       Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"yay someone has been kicked "});
   });

}

/// ** connection event listener ** ///
connectionFailed=()=>{
   Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"connection failed"});
}

connectionDisconnected=()=>{
   Swal.fire({
        position: 'bottom-start',
        icon: 'success',
        title:"Disconnected from conference"});
}

connectionEstablished=()=>{
this.createRoom();
}

///////////////**//////////////////////
connectionEvents=()=>{
   this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED,this.connectionEstablished);
   this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_FAILED,this.connectionFailed);
   this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED,this.connectionDisconnected);

}
    
//// ** Create connection with jitsi meet ** ////
createConnection=()=>{
    JitsiMeetJS.init(this.browserOptions());
    console.log(this.config.server);
    this.connection=new JitsiMeetJS.JitsiConnection(null,null,this.config.server);
    this.connectionEvents();
    this.connection.connect();
   };



//// ** boolean function. Return true if successfully room create ** ////
createRoom=()=>{
    
    this.room=this.connection.initJitsiConference(this.roomName,this.conferenceOption());
    this.conferenceEvents();
    this.joinRoom();
    return true;
   }


//// ** Return true if room join successfull ** ////
joinRoom=(mObj)=>{
    // if(mObj.role==="moderator"){
    //     this.room.join({username:mObj.uname,password:mObj.password});
    // }else
     this.room.join();
     JitsiMeetJS.createLocalTracks({ devices: [ 'audio', 'video' ] })
    .then(this.onLocalTracks)
    .catch(error => {
        throw error;
    });
}

teacherVideo=(x)=>{
    let str= `<div class="video-border" id="video-border${x}">`+
          `<video id="${x}" autoplay="1">`+
            ' </video>'+
            ` <button class="btn btn-outline-secondary" style="z-index: 99;position:absolute;bottom:15px;margin-left: -65%" onclick="Mute('video-border${x}')" ><img src="icons/mute.png" width="16 auto"/></button>`+
         '</div>';
     return str;
 }

 studentsVideo=()=>{
     let str="";
     for(let i=0;i<this.members.length && i<6;i++){
        str+=`<div class="video-border col-sm-4" id="video-border${i}">`+
        `<video id="${this.members[i].id}" autoplay="1">`+
          ' </video>'+
          ` <button class="btn btn-outline-secondary" style="z-index: 99;position:absolute;bottom:15px;margin-left: -65%" onclick="Mute('video-border${this.members[i].id}')" ><img src="icons/mute.png" width="16 auto"/></button>`+
           `<button class="btn btn-outline-danger" style="z-index: 99;position:absolute;bottom:15px;margin-left: -52%"  onclick="Remove('${this.members[i].id}')" ><img src="icons/close.png" width="16 auto"/></button>`+
       '</div>';
     }
     return str;
 }

addTeacher=(x)=>{
    document.getElementById("teacherView").innerHTML=this.teacherVideo(x);
}

addStudents=()=>{
    document.getElementById("frontBanch").innerHTML=this.studentsVideo();
}
Remove=(id)=>{
    this.room.kick(id);
    return true;
}

};


class ConferenceUI extends Conference{
    constructor(option){
        super(option);
    }

    teacherUI=()=>{
        return '<div id="black-box" style="background-color:#2f3640;padding:20px;width:'+this.config.width+';height:'+this.config.height+';"><div id="frontBanch" class="row"></div></div>';
    }

    studentUI=()=>{
        return '<div id="black-box" style="background-color:#2f3640;padding:20px;width:'+this.config.width+';height:'+this.config.height+';"><div id="teacherView" class="col-sm-8" style="padding:20px;display:block;margin:auto;"></div></div>';
    }

     selectView=()=>{
         if(this.role){
             if(this.role==="teacher"){
                 if(this.config.node){
                     document.getElementById(this.config.node).innerHTML=this.teacherUI();
                 }
             }else{
                if(this.config.node){
                    document.getElementById(this.config.node).innerHTML=this.studentUI();
                }
             }
         }
     }

     start=(role)=>{
         this.setRole(role);
         this.selectView();
         this.createConnection();
     }
   
}