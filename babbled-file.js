"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _instanceof(left, right) { if (right != null && typeof Symbol !== "undefined" && right[Symbol.hasInstance]) { return !!right[Symbol.hasInstance](left); } else { return left instanceof right; } }

function _classCallCheck(instance, Constructor) { if (!_instanceof(instance, Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Conference = function Conference(options) {
  var _this = this;

  _classCallCheck(this, Conference);

  _defineProperty(this, "getLocaltracks", function () {
    return _this.localTracks;
  });

  _defineProperty(this, "getConnection", function () {
    return _this.connection;
  });

  _defineProperty(this, "getRoom", function () {
    return _this.room;
  });

  _defineProperty(this, "getMembers", function () {
    return _this.members;
  });

  _defineProperty(this, "setRoomName", function (name) {
    _this.roomName = name;
  });

  _defineProperty(this, "setConfiguration", function (conf) {
    _this.option = conf;
  });

  _defineProperty(this, "setRole", function (value) {
    _this.role = value;
  });

  _defineProperty(this, "browserOptions", function () {
    _this.initOptions = {
      disableAudioLevels: false,
      // The ID of the jidesha extension for Chrome.
      desktopSharingChromeExtId: 'mbocklcggfhnbahlnepmldehdhpjfcjp',
      // Whether desktop sharing should be disabled on Chrome.
      desktopSharingChromeDisabled: false,
      // The media sources to use when using screen sharing with the Chrome
      // extension.
      desktopSharingChromeSources: ['screen', 'window'],
      // Required version of Chrome extension
      desktopSharingChromeMinExtVersion: '0.1',
      // Whether desktop sharing should be disabled on Firefox.
      desktopSharingFirefoxDisabled: true
    };
    return _this.initOptions;
  });

  _defineProperty(this, "conferenceOption", function () {
    _this.confOption = {
      openBridgeChannel: true
    };
    return _this.confOption;
  });

  _defineProperty(this, "onJoinSuccess", function () {
    if (_this.createRoom(_this.roomName)) {
      _this.conferenceEvents();

      _this.joinRoom();
    }
  });

  _defineProperty(this, "localTrack", function (track) {
    _this.localTracks = track;
  });

  _defineProperty(this, "remoteTrack", function (track) {
    if (track.isLocal()) {
      return;
    }

    var participant = track.getParticipantrId();

    if (!remoteTracks[participant]) {
      _this.remoteTracks[participant] = [];
    }

    var idx = remoteTracks[participant].push(track);
    var user = {
      id: participant,
      vid: participant + idx,
      track: track,
      isKicked: false
    };

    _this.members.push(user);

    if (_this.role === "teacher") {
      _this.addStudents();
    } else {
      _this.addTeacher(2);
    } //return function will call here

  });

  _defineProperty(this, "conferenceEvents", function () {
    _this.room.on(JitsiMeetJS.events.conference.TRACK_ADDED, _this.localTrack);

    _this.room.on(JitsiMeetJS.events.conference.TRACK_REMOVED, function () {
      alert("track has removed");
    });

    _this.room.on(JitsiMeetJS.events.conference.USER_JOINED, function () {
      alert("a new user joined ");
    });

    _this.room.on(JitsiMeetJS.events.conference.USER_LEFT, function () {
      alert("someone left the conference");
    });

    _this.room.on(JitsiMeetJS.events.conference.CONFERENCE_JOINED);

    _this.room.on(JitsiMeetJS.events.conference.CONFERENCE_LEFT, function () {
      alert("you have left the conference");
    });

    _this.room.on(JitsiMeetJS.events.conference.CONFERENCE_FAILED, function () {
      alert("you can't join the conference");
    });

    _this.room.on(JitsiMeetJS.events.conference.KICKED, function () {
      alert("yay someone has been kicked ");
    });
  });

  _defineProperty(this, "connectionFailed", function () {
    alert("connection failed");
  });

  _defineProperty(this, "connectionDisconnected", function () {
    alert("Disconnected from conference");
  });

  _defineProperty(this, "connectionEstablished", function () {
    _this.createRoom();
  });

  _defineProperty(this, "connectionEvents", function () {
    _this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_ESTABLISHED, _this.connectionEstablished);

    _this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_FAILED, _this.connectionFailed);

    _this.connection.addEventListener(JitsiMeetJS.events.connection.CONNECTION_DISCONNECTED, _this.connectionDisconnected);
  });

  _defineProperty(this, "createConnection", function () {
    JitsiMeetJS.init(_this.browserOptions());
    console.log(_this.config.server);
    _this.connection = new JitsiMeetJS.JitsiConnection(null, null, _this.config.server);

    _this.connectionEvents();

    _this.connection.connect();
  });

  _defineProperty(this, "createRoom", function (name, mObj) {
    _this.room = _this.connection.initJitsiConference(name, _this.conferenceOption());

    _this.conferenceEvents();

    _this.joinRoom(mObj);

    return true;
  });

  _defineProperty(this, "joinRoom", function (mObj) {
    if (mObj.role === "moderator") {
      _this.room.join({
        username: mObj.uname,
        password: mObj.password
      });
    } else _this.room.join();
  });

  _defineProperty(this, "teacherVideo", function (x) {
    var str = "<div class=\"video-border\" id=\"video-border".concat(x, "\">") + "<video id=\"".concat(x, "\" autoplay=\"1\">") + ' </video>' + " <button class=\"btn btn-outline-secondary\" style=\"z-index: 99;position:absolute;bottom:15px;margin-left: -65%\" onclick=\"Mute('video-border".concat(x, "')\" ><img src=\"icons/mute.png\" width=\"16 auto\"/></button>") + '</div>';
    return str;
  });

  _defineProperty(this, "studentsVideo", function () {
    var str = "";

    for (var i = 0; i < _this.members.length && i < 6; i++) {
      str += "<div class=\"video-border col-sm-4\" id=\"video-border".concat(i, "\">") + "<video id=\"".concat(_this.members[i].id, "\" autoplay=\"1\">") + ' </video>' + " <button class=\"btn btn-outline-secondary\" style=\"z-index: 99;position:absolute;bottom:15px;margin-left: -65%\" onclick=\"Mute('video-border".concat(_this.members[i].id, "')\" ><img src=\"icons/mute.png\" width=\"16 auto\"/></button>") + "<button class=\"btn btn-outline-danger\" style=\"z-index: 99;position:absolute;bottom:15px;margin-left: -52%\"  onclick=\"Remove('".concat(_this.members[i].id, "')\" ><img src=\"icons/close.png\" width=\"16 auto\"/></button>") + '</div>';
    }

    return str;
  });

  _defineProperty(this, "addTeacher", function (x) {
    document.getElementById("teacherView").innerHTML = _this.teacherVideo(x);
  });

  _defineProperty(this, "addStudents", function () {
    document.getElementById("frontBanch").innerHTML = _this.studentsVideo();
  });

  _defineProperty(this, "Remove", function (id) {
    _this.room.kick(id);

    return true;
  });

  //// ** initial variables ** ////
  this.hostObj = {
    host: {
      domain: "192.168.0.150",
      muc: "conference.192.168.0.150",
      anonymousdomain: "192.168.0.150"
    },
    bosh: "192.168.0.150/http-bind"
  };
  this.defaultObj = {
    "server": this.hostObj,
    "width": "100%",
    "height": "100%",
    "node": null,
    "roomName": "abc"
  };
  this.config = { ...this.defaultObj,
    ...options
  };
  this.connection = null;
  this.remoteTracks = null;
  this.room = null;
  this.isJoined = false;
  this.members = [];
  this.roomName = "abc";
  this.localTracks = [];
  this.initOptions = null;
  this.role = null;
} //// ** get set functions  ** ////
// ** method to get localTracks **//
;

;

var ConferenceUI =
/*#__PURE__*/
function (_Conference) {
  _inherits(ConferenceUI, _Conference);

  function ConferenceUI(option) {
    var _this2;

    _classCallCheck(this, ConferenceUI);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(ConferenceUI).call(this, option));

    _defineProperty(_assertThisInitialized(_this2), "teacherUI", function () {
      return '<div style="background-color:#2f3640;padding:20px;width:' + _this2.config.width + ';height:' + _this2.config.height + ';"><div id="frontBanch" class="row"></div></div>';
    });

    _defineProperty(_assertThisInitialized(_this2), "studentUI", function () {
      return '<div style="background-color:#2f3640;padding:20px;width:' + _this2.config.width + ';height:' + _this2.config.height + ';"><div id="teacherView" class="col-sm-8" style="padding:20px;display:block;margin:auto;"></div></div>';
    });

    _defineProperty(_assertThisInitialized(_this2), "selectView", function () {
      if (_this2.role) {
        if (_this2.role === "teacher") {
          if (_this2.config.node) {
            document.getElementById(_this2.config.node).innerHTML = _this2.teacherUI();
          }
        } else {
          if (_this2.config.node) {
            document.getElementById(_this2.config.node).innerHTML = _this2.studentUI();
          }
        }
      }
    });

    _defineProperty(_assertThisInitialized(_this2), "start", function (role) {
      _this2.setRole(role);

      _this2.selectView();

      _this2.createConnection();
    });

    return _this2;
  }

  return ConferenceUI;
}(Conference);